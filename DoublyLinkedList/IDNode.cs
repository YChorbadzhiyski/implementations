﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DoublyLinkedList
{
    public interface IDNode<T>
    {
        public DNode<T> getNext();
        public DNode<T> getPrev();
        public T getData();
        public IDNode<T> InsertNext(T value);
        public IDNode<T> InsertPrev(T value);
        public void RemoveNode(T value);
        public int Count();
        public IDNode<T> TraverseFront(T value);
        public IDNode<T> TraverseBack(T value);
    }
}
