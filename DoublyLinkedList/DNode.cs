﻿using System;

namespace DoublyLinkedList
{
    /// <summary>
    /// Represents double linked list node
    /// </summary>
    /// <typeparam name="T">Class param type</typeparam>
    public class DNode<T>: IDNode<T>
    {
        internal T Data { get; set; }

        internal DNode<T> Prev { get; set; }
        
        internal DNode<T> Next { get; set; }

        public DNode(T value)
        {
            Data = value;
            Next = null;
            Prev = null;
        }

        public DNode<T> getNext()
        {
            return Next;
        }
        public DNode<T> getPrev()
        {
            return Prev;
        }

        public T getData()
        {
            return Data;
        }

        /// <summary>
        /// Inserts new node <see cref="DNode{T}"/> after the current 
        /// </summary>
        /// <param name="value">Value of the node</param>
        /// <returns>Newly added node <see cref="DNode{T}"/></returns>
        /// <exception cref="ArgumentNullException">Null value passed</exception>
        public IDNode<T> InsertNext(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            DNode<T> newNode = new DNode<T>(value);
            newNode.Next = Next;
            newNode.Prev = this;

            if (Next != null)
            {
                this.Next.Prev = newNode;
                this.Next = newNode;
                return newNode;
            }

            this.Next = newNode;
            return newNode;
        }

        /// <summary>
        /// Inserts new node <see cref="DNode{T}"/> before the current 
        /// </summary>
        /// <param name="value">Value of the node</param>
        /// <returns>Newly added node <see cref="DNode{T}"/></returns>
        /// <exception cref="ArgumentNullException">Null value passed</exception>
        public IDNode<T> InsertPrev(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            DNode<T> newNode = new DNode<T>(value);
            newNode.Prev = this.Prev;
            newNode.Next = this;
             
            if (this.Prev != null)
            {
                this.Prev.Next = newNode;
                this.Prev = newNode;
                return newNode;
            }

            this.Prev = newNode;

            return newNode;
        }


        /// <summary>
        /// Deletes a Node with particular Value 
        /// </summary>
        /// <param name="value">Value of the node to be deleted</param>
        public void RemoveNode(T value)
        {
            if (Prev == null)
            {
                if (Data.Equals(value))
                {
                    Next.Prev = null;
                    Next = null;
                    Console.WriteLine($"\nStart of Linked list with value {value} REMOVED \n");
                    return;
                }
            }

            DNode<T> temp = Next;
            while (temp != null)
            {
                if (temp.Data.Equals(value))
                {
                    temp.Prev.Next = temp.Next;
                    if (temp.Next != null)
                    {
                        temp.Next.Prev = temp.Prev;
                    }
                    temp.Next = null; 
                    temp.Prev = null;
                Console.WriteLine($"\nNode with value {value} REMOVED!\n");
                return;
                }

                temp = temp.Next;
            }
            Console.WriteLine($"No node with value {value} was found!");
        }

        /// <summary>
        /// Count the size of the Linked List from the node passed to the end
        /// </summary>
        /// <returns>Integer value representing the size of the linked list</returns>
        public int Count()
        {
            var count = 0;

            DNode<T> temp = this;
            count++;

            while (temp.Next != null)
            {
                count++;
                temp = temp.Next;
            }

            return count;
        }

        /// <summary>
        /// Reads the data from every node starting from the current one until the end of the linked list
        /// </summary>
        public IDNode<T> TraverseFront(T value)
        {
            if (!CheckForDeletedNode()) return null;
            DNode<T> InitialNodeDirection = this.Next;
            bool FrontalDirection = true;
            string message = "Traversing";
            return TraverseHelper(InitialNodeDirection, FrontalDirection, message, value);
        }

        /// <summary>
        /// Reads the data from every node starting from the current one until the beginning of the linked list
        /// </summary>
        public IDNode<T> TraverseBack(T value)
        {
            if (!CheckForDeletedNode()) return null;
            DNode<T> InitialNodeDirection = this.Prev;
            bool FrontalDirection = false;
            string message = "Traversing Backwards";
            return TraverseHelper(InitialNodeDirection, FrontalDirection, message, value);
        }

        /// <summary>
        /// Checks for single nodes without Previous and Next nodes
        /// </summary>
        /// <returns>False if both Prev and Next are null</returns>
        private bool CheckForDeletedNode()
        {
            if (Next == null && Prev == null) return false;
            return true;
        }

        /// <summary>
        /// Visualization helper method that represents the Linked List Data
        /// </summary>
        /// <param name="Node">Takes Previous or Next Node of the indicated one</param>
        /// <param name="direction">Boolean value for direction of the traversion (true - frontal, false - backward)</param>
        private IDNode<T> TraverseHelper(DNode<T> Node, bool direction, string message, T value)
        {
            Console.Write($"{message}: \n{this.Data}");
            DNode<T> temp = Node;
            DNode<T> foundNode = null;

            while (temp != null)
            {
                // checks for the node with that data
                if (temp.getData().Equals(value))
                {
                    foundNode = temp;
                }

                if (direction)
                {
                    Console.Write($" -> {temp.Data}");
                    temp = temp.Next;
                }
                else
                {
                    Console.Write($" -> {temp.Data}");
                    temp = temp.Prev;
                }
            }

            if(foundNode != null)
            {
                Console.WriteLine($"\nNode with value {value} found");
                return foundNode;
            }

            Console.WriteLine();
            //Console.WriteLine($"No node with value {value} was found.");
            return null;
        }
    }
}
