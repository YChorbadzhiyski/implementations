﻿using System;

namespace DoublyLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            DNode<int> node1 = new DNode<int>(1);
            DNode<int> node2 = new DNode<int>(2);

            node1.Next = node2;
            node2.Prev = node1;

            node1.TraverseFront(1);
            node2.TraverseBack(0);

            IDNode<int> node3 = node1.InsertNext(3);
            IDNode<int> node8 = node2.InsertNext(8);

            node1.TraverseFront(0);
            node1.RemoveNode(8);

            node1.TraverseFront(0);
            node8.TraverseBack(0);
            node3.TraverseBack(0);

            IDNode<int> node4 = node1.InsertPrev(4);
            IDNode<int> node5 = node2.InsertPrev(5);
            IDNode<int> node6 = node2.InsertNext(6);


            Console.WriteLine();
            node4.TraverseFront(6);
            node2.TraverseBack(0);
            node3.TraverseFront(0);
            node5.TraverseBack(0);

            Console.WriteLine("\nThe size of the linked list is " + node4.Count());
            Console.ReadLine();
        }
    }
}
