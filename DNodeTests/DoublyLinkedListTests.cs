﻿using DoublyLinkedList;
using NUnit.Framework;

namespace DNodeTests
{
    [TestFixture]
    public class DoublyLinkedListTests
    {
        IDNode<int> head;
        IDNode<int> node2;
        IDNode<int> node3;

        [SetUp]
        public void SetUp()
        {
            head = new DNode<int>(1);
            node2 = head.InsertNext(2);
            node3 = node2.InsertNext(3);
        }

        [Test]
        [Category("InsertNext Method tests")]
        public void InsertNextCreatesNewNonNullNode()
        {
            var actual = node2.getData();
            var expected = 2;

            Assert.IsNotNull(head.getNext());
            Assert.IsNotNull(node2);
            Assert.That(expected, Is.EqualTo(actual));
            Assert.That(head.getNext(), Is.Not.EqualTo(null));
        }

        [Test]
        [Category("InsertNext Method tests")]
        public void InsertNextPlacesNewNodeAfter()
        {
            head = new DNode<int>(1);

            IDNode<int> node2 = head.InsertNext(2);

            Assert.IsNotNull(head.getNext());
            Assert.IsNull(node2.getNext());
        }

        [Test]
        [Category("InsertNext Method tests")]
        public void InsertNEXTBetweenTwoNodesAssignesCorrectRelations()
        {
            head = new DNode<int>(1);
            IDNode<int> endNode = head.InsertNext(2);

            IDNode<int> newNode = head.InsertNext(3);

            Assert.AreEqual(newNode.getPrev(), head);
            Assert.AreEqual(newNode.getNext(), endNode);
            Assert.AreNotEqual(head.getNext(), endNode);
        }

        [Test]
        [Category("InsertNext Method tests")]
        public void InsertNextBadResultOfAddingNewNode()
        {
            head = new DNode<int>(1);
            IDNode<int> endNode = head.InsertNext(2);

            IDNode<int> newNode = head.InsertNext(3);

            Assert.That(newNode.getNext(), Is.Not.Null);
            Assert.That(head.getPrev(), Is.Not.EqualTo(newNode));
            Assert.That(endNode.getPrev(), Is.Not.EqualTo(head));
        }

        [Test]
        [Category("InsertPrev Method tests")]
        public void InsertPrevCreatesNewNonNullNode()
        {
            head = new DNode<int>(1);

            IDNode<int> node2 = head.InsertPrev(2);
            var actual = node2.getData();
            var expected = 2;

            Assert.IsNotNull(head.getPrev());
            Assert.IsNotNull(node2);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [Category("InsertPrev Method tests")]
        public void InsertPrevPlacesNewNodeBefore()
        {
            head = new DNode<int>(1);

            IDNode<int> node2 = head.InsertPrev(2);

            Assert.IsNotNull(head.getPrev());
            Assert.IsNull(node2.getPrev());
        }

        [Test]
        [Category("InsertPrev Method tests")]
        public void InsertPREVBetweenTwoNodesAssignesCorrectRelations()
        {
            head = new DNode<int>(1);
            IDNode<int> endNode = head.InsertNext(2);

            IDNode<int> newNode = head.InsertPrev(3);

            Assert.AreEqual(newNode.getPrev(), null);
            Assert.AreEqual(newNode.getNext(), head);
            Assert.AreEqual(head.getNext(), endNode);
        }

        [Test]
        [Category("InsertPrev Method tests")]
        public void InsertPrevBadResultOfAddingNewNode()
        {
            head = new DNode<int>(1);
            IDNode<int> endNode = head.InsertNext(2);

            IDNode<int> newNode = head.InsertPrev(3);

            Assert.That(newNode.getNext(), Is.Not.EqualTo(endNode));
            Assert.That(head.getPrev(), Is.Not.Null);
            Assert.That(endNode.getPrev(), Is.Not.EqualTo(newNode));
        }

        // Add removeNode test

        [Test]
        [Category("RemoveNode method tests")]
        public void MethodRemovesDesiredNode()
        {
            Assert.That(head.getNext, Is.EqualTo(node2));
            Assert.That(node3.getPrev, Is.EqualTo(node2));

            head.RemoveNode(2);

            Assert.That(head.getNext(), Is.EqualTo(node3));
            Assert.That(node3.getPrev(), Is.EqualTo(head));
            Assert.That(node2.getNext(), Is.Null);
            Assert.That(node2.getPrev(), Is.Null);
        }

        [Test]
        [Category("RemoveNode method tests")]
        public void MethodDoesNotRemoveDesiredNode()
        {
            head.RemoveNode(2);

            Assert.That(head.getNext(), Is.Not.EqualTo(node2));
            Assert.That(node3.getPrev(), Is.Not.EqualTo(node2));
            Assert.That(node2.getNext(), Is.Not.EqualTo(node3));
            Assert.That(node2.getPrev(), Is.Not.EqualTo(head));
        }

        [Test]
        [Category("RemoveNode method tests")]
        public void MethodRemoveHead()
        {
            head.RemoveNode(1);

            Assert.That(head.getNext(), Is.Null);
            Assert.That(node2.getPrev(), Is.Null);
        }

        // Add traverseBackward/Forward tests

        [Test]
        [Category("TraverseFront/Back methods tests")]
        public void TraversingFrontReturnsDesiredNode()
        {
            IDNode<int> returnedNode = head.TraverseFront(2);

            Assert.That(returnedNode.getNext(), Is.EqualTo(node3));
            Assert.That(returnedNode.getPrev(), Is.EqualTo(head));
            Assert.That(returnedNode.getData(), Is.EqualTo(2));
        }

        [Test]
        [Category("TraverseFront/Back methods tests")]
        public void TraversingFrontNotExecutingIfThereIsASingleNode()
        {
            DNode<int> head = new DNode<int>(1);
            IDNode<int> returnedNode = head.TraverseFront(1);

            Assert.That(returnedNode, Is.Null);
        }

        [Test]
        [Category("TraverseFront/Back methods tests")]
        public void TraversingBackReturnsDesiredNode()
        {
            IDNode<int> returnedNode = node3.TraverseBack(1);

            Assert.That(returnedNode.getNext(), Is.EqualTo(node2));
            Assert.That(returnedNode.getPrev(), Is.EqualTo(null));
            Assert.That(returnedNode.getData(), Is.EqualTo(1));
        }

        [Test]
        [Category("TraverseFront/Back methods tests")]
        public void TraversingBackNotExecutingIfThereIsASingleNode()
        {
            DNode<int> head = new DNode<int>(1);
            IDNode<int> returnedNode = head.TraverseBack(1);

            Assert.That(returnedNode, Is.Null);
        }

        // Add Count Tests
        [Test]
        [Category("Count method tests")]
        public void CountGiveCorrectResult()
        {
            var result1 = head.Count();
            var result2 = node2.Count();
            var result3 = node3.Count();

            Assert.That(result1, Is.EqualTo(3));
            Assert.That(result2, Is.EqualTo(2));
            Assert.That(result3, Is.EqualTo(1));
        }

    }
}
